import pymses
import numpy as np
from pymses.sources.ramses.output import *
from pymses.filters import RegionFilter
from pymses.utils.regions import Sphere
from pymses.analysis.visualization import *
from pymses.utils import constants as C
from astropy.io import fits
from pymses.filters import CellsToPoints


output = 23


# Read additional tracers : metallicity
RamsesOutput.amr_field_descrs_by_file = \
{   "3D": {"hydro" : [ Scalar("rho", 0), Vector("vel", [1, 2, 3]), Scalar("P", 4), Scalar("Z", 5) ],\
         "grav"  : [ Vector("g", [0, 1, 2]) ]
        }
}

print  '\n-----------------------------\n    Loading Ramses output\n-----------------------------\n'
ro = pymses.RamsesOutput("/galex_bingo/Simulations/boxlen100_n128_lcdmw5_nonthermal_lessref_zoom-bingo-12/", output)

print '\nSelecting amr fields'
amr = ro.amr_source(["rho", "vel", "P" ,"Z"])


print '\nCreating sphere region'
center = [0.5, 0.5, 0.5]
radius = 0.001
region = Sphere(center, radius)

# AMR data filtering
print '\nFiltering the data with region'
filt_amr = RegionFilter(region, amr)


cell_source = CellsToPoints(filt_amr)


print 'flatten...'
cells = cell_source.flatten()

for i in range(cells.npoints):
        print 'size: ', cells.get_sizes()[i]
        #print 'P = ', cells.fields['P'][i]
        #print 'Pvel = ', cells.fields['Pvel'][i]
        #print 'Z = ', cells.fields['Z'][i]
        #print 'rho = ', cells.fields['rho'][i]
        #print 'center = ', cells.points[i]


