import numpy as np
import math
import matplotlib.pyplot as plt


pathToHalo = '/galex_bingo/Simulations/boxlen100_n128_lcdmw5_nonthermal_lessref_zoom-bingo-12/Halos/23/haloProps.023'
halo = np.genfromtxt(pathToHalo, skip_header = 1)


unit_l      =  0.192771769520447*math.pow(10,27) # box unit to cgs
unit_d      =  0.275150647737918*math.pow(10,-28) # box units to cgs
unit_t      =  0.867491067494782*math.pow(10,17) # box units to cgs

Mpc2cm = 3.08*math.pow(10,24)


class Halo:

	def __init__(self, halo_number, min_part_ID, nb_part, level, nb_host_halo, \
nb_host_subHalo, nb_subHalo_in_halo, nb_nextsubHalo_in_halo, mass, x, y, z, vx, vy, vz):
		self.halo_number = int(halo_number)
		self.min_part_ID = int(min_part_ID)
		self.nb_part = int(nb_part)
		self.level = int(level)
		self.nb_host_halo = int(nb_host_halo)
		self.nb_host_subHalo = int(nb_host_subHalo)
		self.nb_subHalo_in_halo = int(nb_subHalo_in_halo)
		self.nb_nextsubHalo_in_halo = int(nb_nextsubHalo_in_halo)
		self.mass = float(mass)*math.pow(10,11)
		self.x = float(x)*Mpc2cm/unit_l + .5
		self.y = float(y)*Mpc2cm/unit_l + .5
		self.z = float(z)*Mpc2cm/unit_l + .5
		self.vx = float(vx)
		self.vy = float(vy)
		self.vz = float(vz)

	def printHalo(self):
		print 'Halo number         : %i'%self.halo_number
		print 'number of particules: %i'%self.nb_part
		print 'mass                : %e Msun'%self.mass
		print 'position            : (%f,%f,%f)'%(self.x, self.y, self.z)
		print 'velocity            : (%f,%f,%f)'%(self.vx, self.vy, self.vz)

	def printPosition(self):
		print 'position            : (%f,%f,%f)'%(self.x, self.y, self.z)


halos = []

for i in range(len(halo[:,0])):
	h1 = halo[i,:]	
	h = Halo(h1[0],h1[1],h1[2],h1[3],h1[4],h1[5],h1[6],h1[7],h1[8],h1[9],h1[10],h1[11],h1[12],h1[13],h1[14])
	halos.append(h)
x =[]
y =[]
for h in halos:
	h.printPosition() 
	x.append(h.x)
	y.append(h.y)

#plt.plot(x,y, '+')
#plt.show()	

#h.printHalo()





