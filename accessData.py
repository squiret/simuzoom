import pymses
import numpy as np
from pymses.sources.ramses.output import *
from pymses.filters import RegionFilter
from pymses.utils.regions import Sphere
from pymses.analysis.visualization import *
from pymses.utils import constants as C
from astropy.io import fits



output = 23


# Read additional tracers : metallicity
RamsesOutput.amr_field_descrs_by_file = \
{   "3D": {"hydro" : [ Scalar("rho", 0), Vector("vel", [1, 2, 3]), Scalar("P", 4), Scalar("Z", 5) ],\
         "grav"  : [ Vector("g", [0, 1, 2]) ]
        }
}

print  '\n------------------------------\n    Loading Ramses output\n--------------------------------\n'
ro = pymses.RamsesOutput("/galex_bingo/Simulations/boxlen100_n128_lcdmw5_nonthermal_lessref_zoom-bingo-12/", output)

print '\nSelecting amr fields'
amr = ro.amr_source(["rho", "vel", "P" ,"Z"])


print '\nCreating sphere region'
center = [0.5, 0.5, 0.5]
radius = 0.1
region = Sphere(center, radius)

# AMR data filtering
print '\nFiltering the data with region'
filt_amr = RegionFilter(region, amr)


# Defining the camera
print '\nCreating camera'
cam  = Camera(center=center, line_of_sight_axis='z', region_size=[radius, radius],\
    up_vector='y', map_max_size=512, log_sensitive=True, distance = radius, \
far_cut_depth = radius)

# What quantity do we want to look at
print '\nDefining what quantity to look at'
rho_op = ScalarOperator(lambda dset: dset["rho"])

# Create the map
print '\nGenerate the map'
map = SliceMap(filt_amr, cam, rho_op, z=0.04) # create a density slice map at z=0.4 depth position


# save as fits
print '\nSave map as fits'
hdu = fits.PrimaryHDU(map)
hdu.writeto('/home/squiret/work/bingo/trunk/samuel/zoom/map_rho.fits', clobber=True)

print '------------------\n   Exited ok\n-------------'


#########################################################
###    Ray Tracing
#########################################################

print '/nRay Tracing...'

# Map operator : mass-weighted density map
up_func = lambda dset: (dset["rho"]**2)
down_func = lambda dset: (dset["rho"])
#scal_op = FractionOperator(up_func, down_func)
#scal_op = MaxLevelOperator()

class MyTempOperator(Operator):
	def __init__(self):
		def invT_func(dset):
			P = dset["P"]
			rho = dset["rho"]
			r = rho/P
#			print r[(rho<=0.0)+(P<=0.0)]
#			r[(rho<=0.0)*(P<=0.0)] = 0.0
			return r
		d = {"invTemp": invT_func}
		Operator.__init__(self, d, is_max_alos=True)

	def operation(self, int_dict):
			map = int_dict.values()[0]
			mask = (map == 0.0)
			mask2 = map != 0.0
			map[mask2] = 1.0 / map[mask2]
			map[mask] = 0.0
			return map
scal_op = MyTempOperator()



# Map region
center = center
#axes = {"los": np.array([ -0.172935, 0.977948, -0.117099 ])}
axes = {"los": "z"}

# Map processing
rt = raytracing.RayTracer(ro, ["rho", "P"])
for axname, axis in axes.items():
	print axname
	cam  = Camera(center=center, line_of_sight_axis=axis, up_vector="y", region_size=[radius, radius], \
			distance=radius, far_cut_depth=radius, map_max_size=512)
	map = rt.process(scal_op, cam)
	factor = ro.info["unit_density"].express(C.H_cc)
	scale = ro.info["unit_length"].express(C.Mpc)

	# save as fits
	print '\nSave map as fits'
	hdu = fits.PrimaryHDU(map)
	hdu.writeto('/home/squiret/work/bingo/trunk/samuel/zoom/map_rho_rt.fits', clobber=True)










