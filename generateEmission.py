import pymses
import numpy as np
from pymses.sources.ramses.output import *
from pymses.filters import RegionFilter
from pymses.utils.regions import Sphere
from pymses.analysis.visualization import *
from pymses.utils import constants as C
from astropy.io import fits
import math

output = 23

kBoltzmann = 1.3806488*math.pow(10,-16) ### erg/K, CGS units
hydrogenMass = 1.6605402*math.pow(10,-24) ### g, CGS units

### from Grevesse+10
Xsol = 0.7380
Ysol = 0.2484
Zsol = 0.0134




# Read additional tracers : metallicity
RamsesOutput.amr_field_descrs_by_file = \
{   "3D": {"hydro" : [ Scalar("rho", 0), Vector("vel", [1, 2, 3]), Scalar("P", 4), Scalar("Z", 5) ],\
         "grav"  : [ Vector("g", [0, 1, 2]) ]
        }
}

print  '\n------------------------------\n    Loading Ramses output\n--------------------------------\n'
ro = pymses.RamsesOutput("/galex_bingo/Simulations/boxlen100_n128_lcdmw5_nonthermal_lessref_zoom-bingo-12/", output)

info = ro.info

print '\nSelecting amr fields'
amr = ro.amr_source(["rho", "vel", "P" ,"Z"])


print '\nCreating sphere region'
center = [0.532237, 0.473124, 0.414475]
radius = 0.01
region = Sphere(center, radius)

# AMR data filtering
print '\nFiltering the data with region'
filt_amr = RegionFilter(region, amr)



#########################################################
###    Ray Tracing
#########################################################

print '/nRay Tracing...'

def getExtrapolatedEmissivity(n, ToverMu): ### returns emissivity from Cloudy Tables
        return 1.
        

def deriveEmissivity(dset):
        Z = dset["Z"]
        P = dset["P"]
        rho_H_cc = dset["rho"]* info["unit_density"].express(C.H_cc)

        ToverMu = (hydrogenMass/kBoltzmann)*P/rho_H_cc
        nH = (Xsol/hydrogenMass)*rho_H_cc

        solarEm = getExtrapolatedEmissivity(nH, ToverMu)

        return (Z/Zsol)*solarEm


emissivity = ScalarOperator(deriveEmissivity)


# Map region
center = center
#axes = {"los": np.array([ -0.172935, 0.977948, -0.117099 ])}
axes = {"los": "z"}

# Map processing
rt = raytracing.RayTracer(ro, ["P", "rho", "Z"])
for axname, axis in axes.items():
	cam  = Camera(center=center, line_of_sight_axis=axis, up_vector="y", region_size=[radius, radius], \
			distance=radius, far_cut_depth=radius, map_max_size=1024)
	map = rt.process(emissivity, cam)

	# save as fits
	print '\nSave map as fits'
	hdu = fits.PrimaryHDU(map)
	hdu.writeto('/home/squiret/work/bingo/trunk/samuel/zoom/emission.fits', clobber=True)


print 'Done...'








